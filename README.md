# Group 24's EnviroHAT project

This repository stores and manages all the files relating to the development of the HAT. Our EnviroHAT will be a tool to analyse climate and will cater to gardeners, teachers, and anyone else that may have an interest.

The HAT is a device that is installed onto an STM32F0 using wires connected to the debug connector on the microcontroller.
Any issues with the connection process can be emailed to LNDCAI001@myuct.ac.za

## Repository

### CAD

Contains any 3d model or other CAD designs

### Documents

Contains all documentation and datasheets

### Firmware

Contains all software developed for the firmware of the device

### PCB

Contains the KiCAD project, schematics, and libraries for the PCB

### misc

I always find I need a folder to store very minor files that don't fit into the structure and don't require their own structure
