# Group 24's EnviroHAT project
# PCB folder

### Newest 3088 KiCAD folder

Contains the main KiCAD schematic and pcb files. Also Contains footprint library and folder for schematic symbols which can be imported if wanted.

### 3088_Group24_Gerbers

Contains all gerbers and drill files in a zip and pos files and the official JLC bill of materials for uploading on JLC 
